/// Access url endpoint for Web Server.
class RemoteApi {
  /// REST API endpoint for coin api
  static const String endpoint = 'https://rest.coinapi.io/v1';

  /// URL endpoint for coin api websockets url
  static const String webSocket = 'wss://ws-sandbox.coinapi.io/v1/';
}

/// [Exception] thrown for server related errors and device errors
class DeviceException implements Exception {
  /// Constructor for exceptions
  DeviceException(this.message, {this.statusCode = 404});

  /// Convert error messages from api
  factory DeviceException.fromJson(Map<String, String> json,
          {int code = 404}) =>
      DeviceException(json['detail']!, statusCode: code);

  /// Error message
  final String message;

  /// Error code
  final int statusCode;

  @override
  String toString() => message;
}
